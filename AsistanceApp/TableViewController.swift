//
//  TableViewController.swift
//  AsistanceApp
//
//  Created by Jorge Andres Gomez Villamizar on 8/23/19.
//  Copyright © 2019 Maria del Rosario León. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    var dataArray: [(String, String, Bool)]? = nil
    @IBOutlet var tableViewElement: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataArray = [(String, String, Bool)]()
        self.getAllStudentData()
        self.dataArray?.append(("Maria del Rosario", "201512345", true))
        self.dataArray?.append(("Jorge Gomez", "201618492", false))
        print("DATA ARRAY", self.dataArray!)
        self.tableViewElement?.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.dataArray != nil) {
            return self.dataArray!.count + 2
        } else {
            return 2
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "backTabla", for: indexPath)
            return cell
        } else if(indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "exportTabla", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "entradaTabla", for: indexPath)
            let data = dataArray![indexPath.row - 2]
            cell.textLabel?.text = "NAME:\(data.0)  CODE:\(data.1)"
            if(data.2) {
                cell.backgroundColor = #colorLiteral(red: 0.5579367988, green: 0.7647058964, blue: 0.4085022225, alpha: 1)
            } else {
                cell.backgroundColor = #colorLiteral(red: 1, green: 0.3117937451, blue: 0.3207181543, alpha: 1)
            }
            return cell
        }
    }
    
    func getAllStudentData() {
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: [])
            
            guard let container = jsonData as? [String: Any] else {
                print("There is no container")
                return
            }
            
            guard let students = container["documents"] as? [[String: Any]] else {
                print("There is no documents")
                return
            }
            
            for studentData in students {
                guard let container1 = studentData["name"] as? [String: String] else { return }
                guard let container2 = studentData["code"] as? [String: String] else { return }
                self.dataArray?.append((container1["stringValue"]!, container2["stringValue"]!, false))
            }
            print("DATA ARRAY", self.dataArray!)
            self.tableViewElement.reloadData()
        }
        
        task.resume()
    }
    
    @IBAction func exportDataToCSV(_ sender: Any) {
        let filename = "classData.csv"
        let path = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(filename)
        
        var csvDataInText = "name,code,status"
        dataArray?.forEach({student in
            let statusText = student.2 ? "PRESENT" : "ABSENT"
            csvDataInText.append("\n\(student.0),\(student.1),\(statusText)")
        })
        
        do {
            try csvDataInText.write(to: path, atomically: true, encoding: String.Encoding.utf8)
            let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        } catch {
            print("Could not export data as csv")
        }
    }
}
