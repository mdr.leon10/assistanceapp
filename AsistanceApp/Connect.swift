//
//  File.swift
//  AsistanceApp
//
//  Created by Maria del Rosario León on 8/13/19.
//  Copyright © 2019 Maria del Rosario León. All rights reserved.
//

import Foundation
import CoreBluetooth

class Connect : NSObject, CBPeripheralManagerDelegate
{
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print(peripheral.state.rawValue)
    }
    
    let connected = "connected"
    let check = "checkAtendance"
    let disconnected = "disconected"
    var peripheral:CBPeripheralManager? = nil
    var attendanceChecked = false
    let NOT_FOUND = "NOT_FOUND"

    
    override init (){
        super.init()
        peripheral = CBPeripheralManager(delegate: self, queue: nil)
    }
    
    func turnBluetoothOn (Appstatus : String) -> Bool{
        var ret = true
        if Appstatus == connected {
            let service = [CBAdvertisementDataLocalNameKey: "Asistencia Maria del Rosario León"]
            print("It started")
            peripheral?.startAdvertising(service)
            print("entre connected connect")
        }
        else if Appstatus == check
        {
            checkAssistance("201423755")
            print("entre check: ")
            ret = attendanceChecked
        }
        else if Appstatus == disconnected
        {
            peripheral?.stopAdvertising()
            if(peripheral!.isAdvertising) {
                print("Did not stop advertising")
            }
            else {
                print("Did stop! Congratulations")
            }
            print("entre disconnected")

        }
//        print("El estado es: \(Appstatus)")
        return ret
    }
    
    func checkAssistance(_ studentCode: String) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-M-yyyy"
        let dateString = formatter.string(from: date)
        
        let url = URL(string: "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/\(dateString)/students/\(studentCode)")!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: [])
            
            if let dict = jsonData as? [String: Any] {
                if let errorDict = dict["error"] as? [String: Any] {
                    print(errorDict["status"]!)
                    self.attendanceChecked = errorDict["status"] as! String != self.NOT_FOUND
                } else {
                    self.attendanceChecked = true
                }
            } else {
                self.attendanceChecked = true
            }
        }
        
        task.resume()
    }
}
