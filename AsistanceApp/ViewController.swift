//
//  ViewController.swift
//  AsistanceApp
//
//  Created by Maria del Rosario León on 8/12/19.
//  Copyright © 2019 Maria del Rosario León. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let connected = "connected"
    let check = "checkAtendance"
    let disconnected = "disconected"
    var status = ""
    var connect = Connect()

    @IBOutlet weak var RedButton: UIButton!
    @IBOutlet weak var YellowButton: UIButton!
    @IBOutlet weak var GreenButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        status = disconnected
        // Do any additional setup after loading the view.
        applyRoundCorners(RedButton)
        applyRoundCorners(YellowButton)
        applyRoundCorners(GreenButton)
        print("el estado es: \(status)")
    }
    
    func applyRoundCorners (_ object: AnyObject){
        object.layer?.cornerRadius = object.frame.size.width/2
        object.layer?.masksToBounds = true
    }
    
    @IBAction func pushButton(_ sender: UIButton) {
        if status == disconnected {
            status = connected
            if connect.turnBluetoothOn(Appstatus : status)
            {
                print("entre a status == disconnected. status: \(status)")
                RedButton.setTitle("Check Attendance!", for: UIControl.State.normal)
                RedButton.titleLabel?.font = UIFont(name: "MarkerFelt-Wide", size: 21)
            }
        }
        else if status == connected {
            status = check
            if connect.turnBluetoothOn(Appstatus : status)
            {
                print("entre a status == disconnected. status: \(status)")
                RedButton.setTitle("Disconnect!", for: UIControl.State.normal)
                RedButton.titleLabel?.font = UIFont(name: "MarkerFelt-Wide", size: 20)
                RedButton.backgroundColor = UIColor.green
            }
            else
            {
                showAlert()
            }
        }
        else if status == check
        {
            status = disconnected
            if connect.turnBluetoothOn(Appstatus : status)
            {
                print("entre a status == disconnected. status: \(status)")
                RedButton.setTitle("Click me!", for: UIControl.State.normal)
                RedButton.titleLabel?.font = UIFont(name: "MarkerFelt-Wide", size: 26)
                RedButton.backgroundColor = UIColor.red
            }
        }
        
    }
    
    func connectBT ()
    {
        if self.connect.turnBluetoothOn(Appstatus : self.connected)
        {
            status = connected
            RedButton.setTitle("You are still not attending class!", for: UIControl.State.normal)
            RedButton.titleLabel?.font = UIFont(name: "MarkerFelt-Wide", size: 12)

        }
    }
    
    func disconnectBT()
    {
        if connect.turnBluetoothOn(Appstatus : self.disconnected)
        {
            status = disconnected
            print("entre a status == disconnected. status: \(status)")
            RedButton.setTitleColor(UIColor.red, for: UIControl.State.normal)
            RedButton.setTitle("Click me!", for: UIControl.State.normal)
            RedButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
           RedButton.titleLabel?.font = UIFont(name: "MarkerFelt-Wide", size: 26)
            
        }
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "Attendance not checked", message: "Want to try again?", preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: "Disconnect", style: UIAlertAction.Style.default, handler: { _ in
            self.disconnectBT()}))
        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler: { _ in self.connectBT()
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}



